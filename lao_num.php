<?php
function ReadNumber($number)
{
    $position_call = array("ແສນ", "ໝື່ນ", "ພັນ", "ຮ້ອຍ", "ສິບ", "");
    $number_call = array("", "ໜຶ່ງ", "ສອງ", "ສາມ", "ສີ່", "ຫ້າ", "ຫົກ", "ເຈັດ", "ແປດ", "ເກົ້າ");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) return $ret;
    if ($number >= 1000000)
    {
        $ret .= ReadNumber(intval($number / 1000000)) . "ລ້ານ";
        $number = intval(fmod($number, 1000000));
    }

    $divider = 100000;
    $pos = 0;
    while($number > 0)
    {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ຊາວ" :
            ((($divider == 10) && ($d == 1)) ? "" :
                ((($divider == 1) && ($d == 1) && ($ret != "")) ? "ເອັດ" : $number_call[$d]));

        $ret .= $d ? (($divider == 10) && ($d == 2) ? '' : $position_call[$pos]) : "";

        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}

$number = $_GET['num'];
echo number_format($number);
echo '<hr>';
echo ReadNumber($number);
